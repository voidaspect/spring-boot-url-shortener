package com.softserve.internship.urlshortener.config;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.nio.file.Files;

@Profile("mapdb")
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(MapDBProperties.class)
public class MapDBConfig {

    private static final Logger log = LoggerFactory.getLogger(MapDBConfig.class);

    private final MapDBProperties mapDBProperties;

    public MapDBConfig(MapDBProperties mapDBProperties) {
        this.mapDBProperties = mapDBProperties;
    }

    @Bean
    public DB db() throws IOException {
        var path = mapDBProperties.getPath().normalize();
        log.info("Using MapDB store at {}", path);
        var parent = path.getParent();
        if (parent != null) {
            Files.createDirectories(parent);
        }
        var db = DBMaker.fileDB(path.toString())
                .fileMmapEnable()
                .make();
        db.getStore().fileLoad();
        return db;
    }

}
