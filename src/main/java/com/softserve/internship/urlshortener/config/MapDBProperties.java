package com.softserve.internship.urlshortener.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.nio.file.Path;

@ConfigurationProperties(prefix = "mapdb.store")
@ConstructorBinding
public final class MapDBProperties {

    private final Path path;

    public MapDBProperties(Path path) {
        this.path = path;
    }

    public Path getPath() {
        return path;
    }

}
