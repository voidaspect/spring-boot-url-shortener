package com.softserve.internship.urlshortener.model;

import javax.validation.constraints.NotNull;
import java.net.URI;

public final class ShortenURIRequest {

    @NotNull
    private final URI uri;

    public ShortenURIRequest(URI uri) {
        this.uri = uri;
    }

    public URI getUri() {
        return uri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShortenURIRequest that = (ShortenURIRequest) o;

        return uri.equals(that.uri);
    }

    @Override
    public int hashCode() {
        return uri.hashCode();
    }

    @Override
    public String toString() {
        return "ShortenURIRequest{" +
                "uri=" + uri +
                '}';
    }
}
