package com.softserve.internship.urlshortener.controller;

import com.softserve.internship.urlshortener.model.ShortenURIRequest;
import com.softserve.internship.urlshortener.service.URIShortenerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
public class URIShortenerController {

    private final URIShortenerService uriShortenerService;

    public URIShortenerController(URIShortenerService uriShortenerService) {
        this.uriShortenerService = uriShortenerService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<?> shorten(@Valid ShortenURIRequest request, UriComponentsBuilder ucb) {
        String key = uriShortenerService.genKey(request.getUri());
        URI location = ucb.path(key).build().toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{key}")
    public ResponseEntity<?> gotoOrigin(@PathVariable String key) {
        return uriShortenerService.original(key)
                .map(uri -> ResponseEntity.status(HttpStatus.PERMANENT_REDIRECT).location(uri).build())
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{key}")
    public ResponseEntity<?> removeMapping(@PathVariable String key) {
        return uriShortenerService.remove(key)
                .map(uri -> ResponseEntity.noContent().build())
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
