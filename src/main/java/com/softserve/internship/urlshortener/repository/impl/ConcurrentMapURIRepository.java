package com.softserve.internship.urlshortener.repository.impl;

import com.softserve.internship.urlshortener.repository.URIRepository;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.net.URI;
import java.util.Optional;

@Profile("mapdb")
@Repository
public class ConcurrentMapURIRepository implements URIRepository {

    private final DB db;

    private final HTreeMap<String, String> uris;

    public ConcurrentMapURIRepository(DB db) {
        this.db = db;
        this.uris = db
                .hashMap("uris", Serializer.STRING, Serializer.STRING)
                .createOrOpen();
    }

    @Override
    public boolean store(String key, URI value) {
        boolean inserted = uris.putIfAbsent(key, value.toString()) == null;
        if (inserted) {
            db.commit();
        }
        return inserted;
    }

    @Override
    public Optional<URI> get(String key) {
        return Optional.ofNullable(uris.get(key)).map(URI::create);
    }

    @Override
    public Optional<URI> remove(String key) {
        var stored = Optional.ofNullable(uris.remove(key));
        if (stored.isPresent()) {
            db.commit();
        }
        return stored.map(URI::create);
    }

}
