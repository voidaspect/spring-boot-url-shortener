package com.softserve.internship.urlshortener.repository;

import java.net.URI;
import java.util.Optional;

public interface URIRepository {

    boolean store(String key, URI value);

    Optional<URI> get(String key);

    Optional<URI> remove(String key);

}
