package com.softserve.internship.urlshortener.service.impl;

import com.softserve.internship.urlshortener.repository.URIRepository;
import com.softserve.internship.urlshortener.service.URIShortenerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

@Service
public class RandomKeyURIShortenerService implements URIShortenerService {

    private static final Logger log = LoggerFactory.getLogger(RandomKeyURIShortenerService.class);

    @SuppressWarnings("SpellCheckingInspection")
    private static final char[] UNIVERSE = """
            0123456789\
            ABCDEFGHIJKLMNOPQRSTUVWXYZ\
            abcdefghijklmnopqrstuvwxyz\
            """.toCharArray();

    private static final int KEY_LENGTH = 6;

    private final Supplier<Random> random;

    private final URIRepository repository;

    public RandomKeyURIShortenerService(Supplier<Random> random, URIRepository repository) {
        this.random = random;
        this.repository = repository;
    }

    @Autowired
    public RandomKeyURIShortenerService(URIRepository repository) {
        this(ThreadLocalRandom::current, repository);
    }

    @Override
    public String genKey(URI uri) {
        String key;
        while (!repository.store((key = randomKey()), uri)) {
            log.warn("Key collision, re-attempting key generation");
        }
        log.debug("Mapped {} => {}", key, uri);
        return key;
    }

    @Override
    public Optional<URI> original(String key) {
        return repository.get(key);
    }

    @Override
    public Optional<URI> remove(String key) {
        Optional<URI> saved = repository.remove(key);
        saved.ifPresent(uri -> log.debug("Unmapped {} => {}", key, uri));
        return saved;
    }

    private String randomKey() {
        int bound = UNIVERSE.length;
        var key = new char[KEY_LENGTH];
        var rnd = random.get();
        for (int i = 0; i < KEY_LENGTH; i++) {
            key[i] = UNIVERSE[rnd.nextInt(bound)];
        }
        return new String(key);
    }
}
