package com.softserve.internship.urlshortener.service;

import java.net.URI;
import java.util.Optional;

public interface URIShortenerService {

    String genKey(URI uri);

    Optional<URI> original(String key);

    Optional<URI> remove(String key);

}
